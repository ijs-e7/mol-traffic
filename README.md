# MOL traffic

## Running/Requirements

1. Install [Miniconda](https://conda.io/en/latest/miniconda.html)
2. Install [Mamba](https://github.com/mamba-org/mamba) or run:
```bash
conda install -c conda-forge mamba
```
3. Clone a specific branch and move into the directory
```bash
git clone --branch mol-traffic-br1  https://repo.ijs.si/e7/mol-traffic.git mol-traffic-br1
cd mol-traffic-br1
```
4. Create and switch environment
```bash
mamba  env create --prefix ./envs -f environment.yaml
./envs/bin/pip install -e .
conda activate ./envs
```
5. Running Snakemake 
   
- Subset
```bash
snakemake -j20 --configfile workflow/configs/config_subset.yaml --snakefile workflow/Snakefile
```
- Full set
```bash
snakemake -j20 --configfile workflow/configs/config_full.yaml --snakefile workflow/Snakefile
```


### Alternative
If Mamba can be installed in base environment, this is one alternative:

- Ignore point (2.) form above instructions
- After moving into the directory (3.) run:
```bash
conda env create --prefix ./.envs_temp -f workflow/envs/environment_mamba.yaml
conda activate ./.envs_temp
```
- Continue with a step (4.)


## Dataset problems
- Missing year 2014
- Some rows with wrong formatting
