import setuptools

with open("README.md", "r") as f:
    long_description = f.read()

setuptools.setup(
    name="moltraffic",
    version="0.0.1",
    description="MOL traffic library",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    install_requires=[],  # Install with conda
)
