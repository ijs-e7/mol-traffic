import unittest
import pandas as pd

from moltraffic.feature_enginering import one_hot_months, one_hot_hour, one_hot_minute, one_hot_day_of_the_week, \
    sin_cos_hour_min


class OneHotMonth(unittest.TestCase):
    def setUp(self):
        data = [
            [10256, '2013-04-17 20:00:00'],
            [9181, '2013-04-06 15:15:00'],
            [240, '2013-01-03 12:00:00'],
            [11694, '2013-05-02 19:30:00'],
            [10831, '2013-04-23 19:45:00'],
            [4437, '2013-02-16 05:15:00']
        ]

        df = pd.DataFrame(data, columns=['ID', 'Time']).set_index('ID')
        df['Time'] = pd.to_datetime(df['Time'])
        self.df = df

    def test_one_hot_months_1(self):
        r = one_hot_months(self.df)
        assert r[0].tolist() == [0, 0, 1, 0, 0, 0]

    def test_one_hot_months_2(self):
        r = one_hot_months(self.df)
        assert r[1].tolist() == [0, 0, 0, 0, 0, 1]

    def test_one_hot_months_3(self):
        r = one_hot_months(self.df)
        assert sum(r[11].tolist()) == 0


class OneHotHour(unittest.TestCase):
    def setUp(self):
        data = [
            [10256, '2013-04-17 20:00:00'],
            [9181, '2013-04-06 15:15:00'],
            [240, '2013-01-03 12:00:00'],
            [11694, '2013-05-02 19:30:00'],
            [10831, '2013-04-23 19:45:00'],
            [4437, '2013-02-16 05:15:00']
        ]

        df = pd.DataFrame(data, columns=['ID', 'Time']).set_index('ID')
        df['Time'] = pd.to_datetime(df['Time'])
        self.df = df

    def test_one_hot_hour_1(self):
        r = one_hot_hour(self.df)
        assert r[12].tolist() == [0, 0, 1, 0, 0, 0]

    def test_one_hot_hour_2(self):
        r = one_hot_hour(self.df)
        assert r[19].tolist() == [0, 0, 0, 1, 1, 0]

    def test_one_hot_hour_3(self):
        r = one_hot_hour(self.df)
        assert sum(r[7].tolist()) == 0


class OneHotMinute(unittest.TestCase):
    def setUp(self):
        data = [
            [10256, '2013-04-17 20:00:00'],
            [9181, '2013-04-06 15:15:00'],
            [240, '2013-01-03 12:00:00'],
            [11694, '2013-05-02 19:30:00'],
            [10831, '2013-04-23 19:45:00'],
            [4437, '2013-02-16 05:15:00']
        ]

        df = pd.DataFrame(data, columns=['ID', 'Time']).set_index('ID')
        df['Time'] = pd.to_datetime(df['Time'])
        self.df = df

    def test_one_hot_minute_1(self):
        r = one_hot_minute(self.df)
        assert r[0].tolist() == [1, 0, 1, 0, 0, 0]

    def test_one_hot_minute_2(self):
        r = one_hot_minute(self.df)
        assert r[1].tolist() == [0, 1, 0, 0, 0, 1]

    def test_one_hot_minute_3(self):
        r = one_hot_minute(self.df)
        assert r[2].tolist() == [0, 0, 0, 1, 0, 0]

    def test_one_hot_minute_4(self):
        r = one_hot_minute(self.df)
        assert r[3].tolist() == [0, 0, 0, 0, 1, 0]


class OneHotDayOfTheWeek(unittest.TestCase):
    def setUp(self):
        data = [
            [10256, '2013-04-17 20:00:00'],
            [9181, '2013-04-06 15:15:00'],
            [240, '2013-01-03 12:00:00'],
            [11694, '2013-05-02 19:30:00'],
            [10831, '2013-04-23 19:45:00'],
            [4437, '2013-02-16 05:15:00']
        ]

        df = pd.DataFrame(data, columns=['ID', 'Time']).set_index('ID')
        df['Time'] = pd.to_datetime(df['Time'])
        self.df = df

    def test_one_hot_day_of_the_week_1(self):
        r = one_hot_day_of_the_week(self.df)
        assert r[0].tolist() == [0, 0, 0, 0, 0, 0]

    def test_one_hot_day_of_the_week_2(self):
        r = one_hot_day_of_the_week(self.df)
        assert r[1].tolist() == [0, 0, 0, 0, 1, 0]

    def test_one_hot_day_of_the_week_3(self):
        r = one_hot_day_of_the_week(self.df)
        assert r[2].tolist() == [1, 0, 0, 0, 0, 0]

    def test_one_hot_day_of_the_week_4(self):
        r = one_hot_day_of_the_week(self.df)
        assert r[3].tolist() == [0, 0, 1, 1, 0, 0]

    def test_one_hot_day_of_the_week_5(self):
        r = one_hot_day_of_the_week(self.df)
        assert r[4].tolist() == [0, 0, 0, 0, 0, 0]

    def test_one_hot_day_of_the_week_6(self):
        r = one_hot_day_of_the_week(self.df)
        assert r[5].tolist() == [0, 1, 0, 0, 0, 1]

    def test_one_hot_day_of_the_week_7(self):
        r = one_hot_day_of_the_week(self.df)
        assert r[6].tolist() == [0, 0, 0, 0, 0, 0]


class SinCosHourMin(unittest.TestCase):
    def setUp(self):
        data = [
            [10256, '2013-04-17 20:00:00'],
            [9181, '2013-04-06 15:15:00'],
            [240, '2013-01-03 12:00:00'],
            [11694, '2013-05-02 19:30:00'],
            [10831, '2013-04-23 19:45:00'],
            [4437, '2013-02-16 05:15:00']
        ]

        df = pd.DataFrame(data, columns=['ID', 'Time']).set_index('ID')
        df['Time'] = pd.to_datetime(df['Time'])
        self.df = df

    def test_sin_cos_hour_min_1(self):
        r = sin_cos_hour_min(self.df)
        assert r[0].tolist() == [0, 0, 0, 0, 0, 0]

    def test_sin_cos_hour_min_2(self):
        r = sin_cos_hour_min(self.df)
        assert r[1].tolist() == [0, 0, 0, 0, 1, 0]
