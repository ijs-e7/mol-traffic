rule figure_missing_data:
    input: "data/interim/minute_full.csv"
    output: report("reports/figures/missing_data.png")
    threads: 1
    shell: "python scripts/figure_missing_data.py -i {input} -o {output}"

rule figure_daily_mean:
    input: "data/interim/minute_full.csv"
    output: report("reports/figures/daily_mean.pdf")
    threads: 1
    shell: "python scripts/daily_mean.py -i {input} -o {output}"

rule figure_monthly_mean_over_years:
    input: "data/interim/minute_full.csv"
    output: report("reports/figures/monthly_mean_over_years.pdf")
    threads: 1
    shell: "python scripts/monthly_mean_over_years.py -i {input} -o {output}"

rule figure_monthly_mean:
    input: "data/interim/minute_full.csv"
    output: report("reports/figures/monthly_mean.pdf")
    threads: 1
    shell: "python scripts/monthly_mean.py -i {input} -o {output}"

rule figure_missing_data_station_merged:
    input: "data/interim/minute_full_station_merged.csv"
    output: report("reports/figures/missing_data_station_merged.png")
    threads: 1
    shell: "python scripts/figure_missing_data.py -i {input} -o {output}"

rule metrics_viz:
    input : predictions="models/{test_or_train}_predictions/{model_name}/subset_{split_date}.csv"
    output: metrics_viz=directory("reports/figures/{test_or_train}_distributions/{model_name}/subset_{split_date}")
    threads: 1
    shell: "python scripts/metrics_distribution.py --predictions {input.predictions} --metrics_viz {output.metrics_viz}"

rule figure_metric_distribution:
    # input: expand("reports/figures/{test_or_train}_distributions/{model_name}/subset_{split_date}", test_or_train=['test', 'train'], split_date=config["train_test_splits"], model_name=MOLModel.get_registered_models())
    input: []

rule mse_details:
    input: "models/{test_or_train}_predictions/{model_name}/subset_{split_date}_run_{run}.csv"
    output: report(directory("reports/figures/mse_{test_or_train}_detail_{model_name}_subset_{split_date}_run_{run}"), patterns=["{name}.png", "{name}.pdf"], category="Notebook vis")
    threads: 1
    shell: "python scripts/metrics_viz.py --predictions {input} --output {output} --figures_type " + config['figures_type']

rule mse_details_all:
    # Only one split date to reduce number of figures
    input: expand("reports/figures/mse_{test_or_train}_detail_{model_name}_subset_{split_date}_run_1", test_or_train=['test', 'train'], split_date=['1_1_2018'], model_name=config["train_models"])
    # input: expand("reports/figures/mse_{test_or_train}_detail_{model_name}_subset_{split_date}", test_or_train=['test', 'train'], split_date=config["train_test_splits"], model_name=MOLModel.get_registered_models())
    # input: []

rule model_error_viz:
    input: "models/{test_or_train}_metrics/summary.yaml"
    output: report("reports/figures/{test_or_train}_metrics_summary.pdf")
    threads: 1
    shell: "python scripts/model_error_viz.py " \
                "--summary_file {input} " \
                "--output {output} "

rule model_error_viz_all:
    input: expand("reports/figures/{test_or_train}_metrics_summary.pdf", test_or_train=['test', 'train'])

rule all_viz:
    input: rules.model_error_viz_all.input + rules.mse_details_all.input + rules.figure_metric_distribution.input + rules.figure_missing_data.output + rules.figure_daily_mean.output + rules.figure_monthly_mean_over_years.output + rules.figure_monthly_mean.output + rules.figure_missing_data_station_merged.output

rule train_meta_postprocessing:
    input: "models/trained_model/{model_name}/train_meta_subset_{split_date}_run_{run}.p"
    output: report(directory("reports/meta/{model_name}/train_meta_subset_{split_date}_run_{run,\d+}"), patterns=["*.png", "*.pdf"], category="Train meta")
    threads: 1
    shell: "python scripts/model_training_meta_analysis.py --input {input} --output_dir {output}"

rule train_meta_postprocessing_all:
    input: expand("reports/meta/{model_name}/train_meta_subset_{split_date}_run_{run}", \
                   split_date=config["train_test_splits"], \
                   model_name=config["train_models"], \
                   run=config["runs"])
