.PHONY: env_base, env, run_subset, run_full, run_paper, report_subset, report_full, report_paper, notebook, flake, doctest, envc, all_clean

env_base:
	mamba env create --prefix ./envs -f environment.yaml

env:
	mamba env create --prefix ./envs -f environment.yaml
	./envs/bin/pip install -e .

envc:
	conda env create --prefix ./envs -f environment.yaml
	./envs/bin/pip install -e .

run_subset:
	snakemake -j 2 --cores 4 --configfile workflow/configs/config_subset.yaml --snakefile workflow/Snakefile

run_full:
	snakemake -j --cores 40 --configfile workflow/configs/config_full.yaml --snakefile workflow/Snakefile -F

run_paper:
	snakemake -j --cores 40 --configfile workflow/configs/config_paper.yaml --snakefile workflow/Snakefile
	
report_subset:
	snakemake --report reports/workflow_report.html --configfile workflow/configs/config_subset.yaml

report_full:
	snakemake --report reports/workflow_report.html --configfile workflow/configs/config_full.yaml

report_paper:
	snakemake --report reports/workflow_report.html --configfile workflow/configs/config_paper.yaml

notebook:
	jupyter-lab

flake:
	flake8 .

doctest:
	find ./scripts/ -name "*.py" -exec python -m doctest -v {} +
	find ./moltraffic/ -name "*.py" -exec python -m doctest -v {} +

all_clean:
	rm -rf data models reports

figures:
	drawio -x --crop -f pdf -o paper/ paper/
