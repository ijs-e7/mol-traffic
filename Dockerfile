FROM continuumio/miniconda3:4.9.2
ADD environment.yaml /tmp/environment.yaml
RUN apt-get --allow-releaseinfo-change update && apt-get install -y \
  unzip \
  curl \
  make \
  git

RUN conda install -c conda-forge -y mamba && \
    mamba env update --file /tmp/environment.yaml --name root && \
    conda clean --all -y
