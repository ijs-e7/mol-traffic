import argparse
import pickle
import pandas as pd
from datetime import timedelta


def transform_times(df):
    df['Time'] = pd.to_datetime(df['Time'])
    return df


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--data', type=str, required=True)
    parser.add_argument('--max_train_size_days', type=int, required=False, default=None)
    parser.add_argument('-r', '--train', type=str, required=True)
    parser.add_argument('-e', '--test', type=str, required=True)
    parser.add_argument('-m', '--meta', type=str, required=True)
    parser.add_argument('--date', type=str, required=True)
    args = parser.parse_args()

    df = (
        pd.read_csv(args.data)
        .pipe(transform_times)
    )

    df_meta = df[['STM', 'DIR', 'NAME']].drop_duplicates().values

    split_date = pd.to_datetime(args.date, format='%d_%m_%Y')
    df_train = df[df['Time'] < split_date]
    if args.max_train_size_days is None:
        df_test = df[df['Time'] >= split_date]
    else:
        selector = (df['Time'] >= split_date) & (df['Time'] < (split_date + timedelta(days=args.max_train_size_days)))
        df_test = df[selector]

    df_train.to_csv(args.train, index=False)
    df_test.to_csv(args.test, index=False)
    pickle.dump({'stations': df_meta, 'split_date': args.date}, open(args.meta, "wb"))
