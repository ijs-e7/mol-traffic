import argparse
import pandas as pd

from moltraffic.data_transformations import drop_nan_STM, add_full_name_column


def flatten(df):
    df1 = df[['newSTM', 'newDIR', 'newNAME', 'newSRC', 'OldName1']]
    df2 = df[['newSTM', 'newDIR', 'newNAME', 'newSRC', 'OldName2']]
    df3 = df[['newSTM', 'newDIR', 'newNAME', 'newSRC', 'OldName3']]
    df1 = df1[df1['OldName1'].notna()].rename(columns={'OldName1': 'FULL'})
    df2 = df2[df2['OldName2'].notna()].rename(columns={'OldName2': 'FULL'})
    df3 = df3[df3['OldName3'].notna()].rename(columns={'OldName3': 'FULL'})
    rdf = pd.concat([df1, df2, df3])
    rdf['FULL'] = rdf['FULL'].str.replace(' ', '')
    return rdf


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Parsing minute data.')
    parser.add_argument('-i', '--input', type=str, required=True)
    parser.add_argument('-m', '--mappings', type=str, required=True)
    parser.add_argument('-o', '--output', type=str, required=True)
    parser.add_argument('-t', '--threads', type=int, required=False, default=1)
    args = parser.parse_args()

    df_data = (
       pd.read_csv(args.input)
       .pipe(add_full_name_column)
    )

    df_mappings = (
        pd.read_csv(args.mappings)
        .pipe(drop_nan_STM)
        .pipe(flatten)
    )

    df_merged = (
        pd.merge(df_data, df_mappings, how="inner", on='FULL')
        .drop(columns=['STM', 'DIR', 'NAME', 'SRC'])
        .rename(columns={'newSTM': 'STM', 'newDIR': 'DIR', 'newNAME': 'NAME', 'newSRC': 'SRC'})
    )

    column_order = list(df_merged.columns)
    column_order.remove('STM')
    column_order.remove('DIR')
    column_order = ['STM', 'DIR'] + column_order
    df_merged.to_csv(args.output, index=False, columns=column_order)
