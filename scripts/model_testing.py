import argparse
import pickle
import time
import pandas as pd
import yaml
from moltraffic.data_transformations import load_weather_data, keep_columns_that_were_in_train_zero_rest, \
    load_holiday_data
from moltraffic.models.models import MOLModel


def write_metrics_file(stats, file):
    with open(file, 'w') as outfile:
        yaml.dump(stats, outfile, default_flow_style=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--test', type=str, required=True)
    parser.add_argument('--metrics_file', type=str, required=True)
    parser.add_argument('--model', type=str, required=True)
    parser.add_argument('--meta', type=str, required=True)
    parser.add_argument('--weather', type=str, required=False)
    parser.add_argument('--holidays', type=str, required=False)
    parser.add_argument('--predictions', type=str, required=True)
    parser.add_argument('--run', type=int, required=False, default=None)
    args = parser.parse_args()

    meta = pickle.load(open(args.meta, "rb"))

    # Load functions from meta. This enforces that train and test use same functions
    dataset_loading_function = meta['dataset_loading_function']
    dataset_transformation_function = meta['dataset_transformation_function']

    # TODO: There should be no aggfunc. Only one value per box
    X_df, Y_df = dataset_loading_function(args.test)
    # X_df_no_dst = daylight_savings_time_transform(X_df.copy())

    weather_df = load_weather_data(args.weather)
    holidays_df = load_holiday_data(args.holidays)

    X_df_transformed_features = dataset_transformation_function(X_df, weather_df, holidays_df)

    model_class = MOLModel.get_registered_model(meta['model_name'])
    model = model_class.load_model(args.model)

    assert model is not None, f'Model {meta["model_name"]} not loaded from {args.model}'

    # Use only columns that were used for training
    Y_columns = meta['output_columns']
    Y = keep_columns_that_were_in_train_zero_rest(Y_columns, Y_df)

    X = X_df_transformed_features[meta['input_columns']].to_numpy()

    start = time.time()
    Y_ = model.predict(X)
    end = time.time()
    test_time = end - start

    stats = {
        'model_name': meta['model_name'],
        'split_date': meta['split_date'],
        'train_time': meta['train_time'],
        'test_time': test_time
    }

    if args.run is not None:
        stats['run'] = args.run

    write_metrics_file(stats, args.metrics_file)

    predicted_pdf = pd.DataFrame(data=Y_, columns=[f'{x}_pred' for x in Y_columns])
    actual_pdf = Y_df.rename(columns={x: f'{x}_actual' for x in Y_columns})

    #pdf_concat = pd.concat([X_df, X_df_transformed_features, actual_pdf, predicted_pdf], axis=1)
    pdf_concat = pd.concat([X_df, actual_pdf, predicted_pdf], axis=1)
    pdf_concat.to_csv(args.predictions, columns=sorted(pdf_concat.columns), compression='gzip')
