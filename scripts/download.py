import argparse
import os
import sys
from pathlib import Path
import owncloud

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Download files from Nextcloud.')
    parser.add_argument('-p', '--password', type=str,
                        required=False,
                        default=os.environ.get('NEXTCLOUD_PASSWORD', None),
                        help='Password of the account')
    parser.add_argument('-u', '--username', type=str,
                        required=False,
                        default=os.environ.get('NEXTCLOUD_USERNAME', None),
                        help='Account username')
    parser.add_argument('-r', '--remote', type=str,
                        required=True,
                        help='Remote file')
    parser.add_argument('-l', '--local', type=str,
                        required=True,
                        help='Local file')
    args = parser.parse_args()
    if args.username is None or args.password is None:
        sys.exit('No username or password')

    # Login
    oc = owncloud.Client('https://portal.ijs.si/nextcloud')
    oc.login(args.username, args.password)

    # Create directory
    directory = os.path.dirname(args.local)
    Path(directory).mkdir(parents=True, exist_ok=True)

    # Download file
    oc.get_file(remote_path=args.remote, local_file=args.local)
