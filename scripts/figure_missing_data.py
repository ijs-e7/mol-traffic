import argparse
from collections import defaultdict
from datetime import timedelta
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np


def all_times(earliest, latest):
    all_times = []
    current_time = earliest
    while current_time < latest:
        all_times.append(current_time)
        current_time += timedelta(minutes=15)
    all_times.append(current_time)
    return all_times


def merge_times(times):
    new_times = []
    start_i = 0
    for i in range(1, len(times)):
        dt = times[i] - times[i - 1]
        if dt != timedelta(minutes=15):
            new_times.append((times[start_i], times[i - 1]))
            start_i = i
    new_times.append((times[start_i], times[-1]))
    return new_times


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Missing data plot')
    parser.add_argument('-i', '--input', type=str, required=True)
    parser.add_argument('-o', '--output', type=str, required=True)
    args = parser.parse_args()

    df = pd.read_csv(args.input)

    df['Time'] = pd.to_datetime(df['Time'])
    df['Year'] = df['Time'].dt.year
    df['Month'] = df['Time'].dt.month

    time_earliest = min(df['Time'])
    time_latest = max(df['Time'])

    all_times_list = all_times(time_earliest, time_latest)

    all_times_dict = defaultdict(lambda: 0)
    for x in all_times_list:
        all_times_dict[(x.year, x.month)] += 1

    stations = set()
    dates = set()
    existing_percentage_dict = defaultdict(lambda: 0)

    gdf = df.groupby(['STM', 'NAME', 'DIR', 'Year', 'Month', 'SRC']).size().reset_index()

    for i, v in gdf.iterrows():
        month = int(v['Month'])
        year = int(v['Year'])
        count = int(v[0])
        filtered_times_counter = all_times_dict[(year, month)]
        existing_percentage = count / filtered_times_counter
        station_pair = (v['SRC'], v['STM'], v['NAME'], v['DIR'])
        date_pair = (year, month)
        stations.add(station_pair)
        dates.add(date_pair)
        existing_percentage_dict[(station_pair, date_pair)] = existing_percentage

    stations = sorted(list(stations))
    all_times_list_subset = sorted(list({(x.year, x.month) for x in all_times_list}))
    X = np.empty((len(all_times_list_subset), len(stations)))
    for j, s in enumerate(stations):
        for i, d in enumerate(all_times_list_subset):
            X[i, j] = existing_percentage_dict[(s, d)]

    ax = sns.heatmap(X.T, linewidths=.1)
    station_labels = [x[0] + ':' + x[1] + '-' + x[2] + '-' + x[3] for x in stations]
    plt.yticks(np.arange(.5, len(stations) + 0.5, 1.0), station_labels, fontsize=2)
    ax.set_yticklabels(ax.get_yticklabels(), rotation=0)

    new_x_tickers = [str(x[0]) + ' ' + str(x[1]) for x in all_times_list_subset]
    plt.xticks(np.arange(.5, len(all_times_list_subset) + 0.5, 1.0), new_x_tickers, fontsize=3)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=90)

    plt.savefig(args.output, bbox_inches='tight', pad_inches=0, dpi=3500)
