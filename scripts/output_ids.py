import argparse
import pandas as pd

from moltraffic.data_transformations import add_full_name_column, transform_times

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='List of all STMs')
    parser.add_argument('-i', '--input', type=str, required=True)
    parser.add_argument('-o', '--output', type=str, required=True)
    args = parser.parse_args()

    df = (
        pd.read_csv(args.input)
        .pipe(add_full_name_column)
        .pipe(transform_times)
    )

    ids = sorted(list(df['FULL'].unique()))

    data = []
    for stm_id in ids:
        filtered = df[df['FULL'] == stm_id]
        first_time = min(filtered['Time'])
        last_time = max(filtered['Time'])
        data.append((stm_id, first_time, last_time))

    write_df = pd.DataFrame(data, columns=['FULL', 'First', 'Last'])
    write_df.to_csv(args.output, header=True)
