import argparse
from collections import defaultdict
import pandas as pd
import yaml

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--statistics', type=str, required=True)
    parser.add_argument('-o', '--output', type=str, required=True)
    args = parser.parse_args()

    dc = defaultdict(lambda: [])
    with open(args.statistics, 'r') as yaml_file:
        d = yaml.load(yaml_file, Loader=yaml.FullLoader)
        for stat in d:
            for k, v in stat.items():
                dc[k].append(v)
    pd.DataFrame(dc).to_latex(args.output)
