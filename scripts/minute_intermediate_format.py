import argparse
import glob
import multiprocessing as mp
import os
from pathlib import Path
from functools import partial
from moltraffic.parsing import parse_minute_file, parse_minute_file_excel


def file_transform(parse_data_function, arguments, file):
    try:
        save_file = os.path.splitext(file.replace(arguments.input, arguments.output))[0] + '.csv'
        directory = os.path.dirname(save_file)
        Path(directory).mkdir(parents=True, exist_ok=True)
        df = parse_data_function(file, arguments.log)
        if df is not None:
            df.to_csv(save_file, index=False)
    except Exception as e:
        print(f'Error while parsing file {file}')
        raise e


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Parsing minute data.')
    parser.add_argument('--input', type=str, required=True)
    parser.add_argument('--output', type=str, required=True)
    parser.add_argument('--log', type=str, required=False, default=None)
    parser.add_argument('--threads', type=int, required=False, default=1)
    args = parser.parse_args()

    files = list(glob.glob(f'{args.input}/*/*.S_M'))
    excel_files = list(glob.glob(f'{args.input}/*/*.xlsx'))

    try:
        if args.log is not None:
            os.remove(args.log)
    except OSError:
        print('Removing old parsing logs.')

    file_transform_csv = partial(file_transform, parse_minute_file, args)

    pool = mp.Pool(args.threads)
    pool.map(file_transform_csv, files)

    file_transform_excel = partial(file_transform, parse_minute_file_excel, args)

    pool = mp.Pool(args.threads)
    pool.map(file_transform_excel, excel_files)
