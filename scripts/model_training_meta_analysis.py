import argparse
from pathlib import Path
import pickle
import matplotlib.pyplot as plt


def create_directory_if_not_exist(directory):
    Path(directory).mkdir(parents=True, exist_ok=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', type=str, required=True)
    parser.add_argument('--output_directory', type=str, required=True)
    args = parser.parse_args()

    create_directory_if_not_exist(args.output_directory)

    meta = pickle.load(open(args.input, "rb"))

    i = 0
    while f'validation_loss_{i}' in meta.keys() and f'train_loss_{i}' in meta.keys():
        lval = meta[f'validation_loss_{i}']
        ltrain = meta[f'train_loss_{i}']
        plt.plot(lval, label=f'val_{i}')
        plt.plot(ltrain, label=f'train_{i}')
        plt.legend()
        plt.savefig(args.output_directory + f'/loss_{i}.pdf', bbox_inches='tight', pad_inches=0)
        i += 1
