import argparse
import pandas as pd
import yaml

from moltraffic.errors import MeanAbsoluteError, MeanSquaredError, MeanAbsoluteErrorIndividualVehicleSumPredictions, \
    MeanAbsoluteErrorIndividualVehiclePredictions


def write_metrics_file(stats, file):
    with open(file, 'w') as outfile:
        yaml.dump(stats, outfile, default_flow_style=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--predictions', type=str, required=True)
    parser.add_argument('--metrics', type=str, required=True)
    parser.add_argument('--test_stats', type=str, required=True)
    args = parser.parse_args()

    df = pd.read_csv(args.predictions, compression='gzip')
    stations = set([x.split('(')[1].split(')')[0] for x in df.columns if '_pred' in x])
    types = set([x.split('(')[0] for x in df.columns if '_pred' in x])

    columns = []
    for station in stations:
        for typ in types:
            column_actual = f'{typ}({station})_actual'
            column_predicted = f'{typ}({station})_pred'
            if column_actual in df.columns and column_predicted in df.columns:
                columns.append(f'{typ}({station})')
            else:
                # Warning
                pass

    Y = df[[f'{x}_actual' for x in columns]].to_numpy()
    Y_ = df[[f'{x}_pred' for x in columns]].to_numpy()
    errors = [MeanAbsoluteError(Y, Y_),
              MeanSquaredError(Y, Y_),
              MeanAbsoluteErrorIndividualVehiclePredictions(Y, Y_, column_names=columns),
              MeanAbsoluteErrorIndividualVehicleSumPredictions(Y, Y_, column_names=columns)]

    stats = None
    with open(args.test_stats, 'r') as yaml_file:
        stats = yaml.load(yaml_file, Loader=yaml.FullLoader)

    for error in errors:
        for error_name, error_value in error.get_errors():
            stats[error_name] = error_value

    write_metrics_file(stats, args.metrics)
