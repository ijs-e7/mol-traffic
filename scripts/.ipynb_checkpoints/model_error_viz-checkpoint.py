import argparse
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from yaml import safe_load

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--summary_file', type=str, required=True)
    parser.add_argument('--output', type=str, required=True)
    args = parser.parse_args()

    with open(args.summary_file, 'r') as f:
        df = pd.json_normalize(safe_load(f))
        sns.barplot(x="model_name", y="MAE", hue="split_date", data=df)
        plt.xticks(rotation=90)
        plt.grid(axis='y', color='0.95')
    plt.savefig(args.output, bbox_inches='tight', pad_inches=0)
