import argparse
import pickle
import time
from moltraffic.data_transformations import load_weather_data, load_holiday_data, all_data_loading_function, \
    get_data_loading_function
from moltraffic.feature_enginering import feature_creation
from moltraffic.models.models import MOLModel
from pathlib import Path


def create_directory_if_not_exist(directory):
    Path(directory).mkdir(parents=True, exist_ok=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--train', type=str, required=True)
    parser.add_argument('--model', type=str, required=True)
    parser.add_argument('--model_name', type=str, required=True)
    parser.add_argument('--meta', type=str, required=True)
    parser.add_argument('--train_meta', type=str, required=True)
    parser.add_argument('--data_loading_function', choices=all_data_loading_function(), type=str.lower, required=True)
    parser.add_argument('--weather', type=str, required=False)
    parser.add_argument('--holidays', type=str, required=False)
    parser.add_argument('--threads', type=int, required=False, default=1)
    args = parser.parse_args()

    input_meta = pickle.load(open(args.meta, "rb"))

    dataset_loading_function = get_data_loading_function(args.data_loading_function)
    dataset_transformation_function = feature_creation
    # TODO: There should be no aggfunc. Only one value per box
    X_df, Y_df = dataset_loading_function(args.train)

    weather_df = load_weather_data(args.weather)
    holidays_df = load_holiday_data(args.holidays)

    X_df_transformed_features = dataset_transformation_function(X_df, weather_df, holidays_df)
    X = X_df_transformed_features.to_numpy()
    Y_df_columns = Y_df.columns.to_list()
    Y = Y_df[Y_df_columns].to_numpy()

    model = MOLModel.get_registered_model(args.model_name)()

    start = time.time()
    model_training_meta = model.fit(X, Y, args.threads)
    end = time.time()
    train_time = end - start

    create_directory_if_not_exist(args.model)
    model.save(args.model)

    output_meta = {
        'input_columns': X_df_transformed_features.columns.to_list(),
        'output_columns': Y_df_columns,
        'model_name': args.model_name,
        'dataset_transformation_function': dataset_transformation_function,
        'dataset_loading_function': dataset_loading_function,
        'split_date': input_meta['split_date'],
        'train_time': train_time
    }

    if model_training_meta is not None:
        output_meta = {**output_meta, **model_training_meta}

    pickle.dump(output_meta, open(args.train_meta, "wb"))
