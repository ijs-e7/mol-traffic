import argparse
import glob
import pandas as pd
import multiprocessing as mp


def read(file):
    return pd.read_csv(file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Merge multiple dataframes.')
    parser.add_argument('-i', '--input', type=str, required=True)
    parser.add_argument('-o', '--output', type=str, required=True)
    parser.add_argument('-t', '--threads', type=int, required=False, default=1)
    parser.add_argument('--ignore_years', nargs='+', required=False, default=[])
    args = parser.parse_args()

    pool = mp.Pool(args.threads)
    files = list(glob.glob(args.input + '/*/*.csv'))
    data = pool.map(read, files)
    full_df = pd.concat(data)

    selector = pd.to_datetime(full_df['Time']).dt.year.isin(int(x) for x in args.ignore_years)
    full_df[~selector].to_csv(args.output, index=False)
