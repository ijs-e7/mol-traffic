import argparse
import glob
import os
import ntpath
import owncloud

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Upload files to Nextcloud.')
    parser.add_argument('-p', '--password', type=str, required=False, default=os.environ.get('NEXTCLOUD_PASSWORD', ''),
                        help='Password of the account')
    parser.add_argument('-u', '--username', type=str, required=False, default=os.environ.get('NEXTCLOUD_USERNAME', ''),
                        help='Account username')
    parser.add_argument('-f', '--files', type=str, required=True, help='Files/file pattern')
    parser.add_argument('-d', '--directory', type=str, required=True, help='Directory')
    args = parser.parse_args()

    oc = owncloud.Client('https://portal.ijs.si/nextcloud')
    oc.login(args.username, args.password)
    for file in glob.glob(args.files):
        print("Uploading", ntpath.basename(file), 'to', f'{args.directory}')
        oc.put_file(f'{args.directory}/{ntpath.basename(file)}', file)
