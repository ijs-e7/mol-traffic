import argparse
import pandas as pd
from moltraffic.visualisations import plot_mean_hourly_vehicles, grouped_mean_data_plot


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Graph monthly use.')
    parser.add_argument('-i', '--input', type=str, required=True)
    parser.add_argument('-o', '--output', type=str, required=True)
    args = parser.parse_args()

    df = pd.read_csv(args.input)
    grouped_mean_data_plot(df, args.output, grouping_function=plot_mean_hourly_vehicles)
