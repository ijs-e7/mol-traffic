from __future__ import print_function
import argparse
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import pathlib
from moltraffic.visualisations import station_error_boxplot, error_distribution, mean_error_over_time, \
    error_distribution_over_time, actual_vs_predicted_series

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--predictions', type=str, required=True)
    parser.add_argument('--output', type=str, required=True)
    parser.add_argument('--figures_type', type=str, required=False, default='pdf')
    args = parser.parse_args()

    fig_type = args.figures_type
    pathlib.Path(args.output).mkdir(parents=True, exist_ok=True)

    predictions_file = args.predictions
    df = pd.read_csv(predictions_file, compression='gzip')
    df['Time'] = pd.to_datetime(df['Time'])

    columns_pred = [x.replace('_pred', '') for x in df.columns if x.endswith('_pred')]
    columns_actual = [x.replace('_actual', '') for x in df.columns if x.endswith('_actual')]

    columns = sorted(list(set(columns_pred).intersection(set(columns_actual))))
    diff_columns = []
    for c in columns:
        df[f'{c}_diff'] = df[f'{c}_pred'] - df[f'{c}_actual']
        diff_columns.append(f'{c}_diff')
    df_diff = df[['Time'] + [x for x in df.columns if x.endswith('_diff')]]

    station_error_boxplot(df_diff[diff_columns], output_file=args.output + f'/station_error.{fig_type}')

    error_distribution(df_diff[diff_columns], output_file=args.output + f'/histplot.{fig_type}')

    mean_error_over_time(df_diff, diff_columns, output_file=args.output + f'/error_plots_over_time.{fig_type}')

    # all_errors_over_time(df_diff, diff_columns, output_file=args.output + f'/all_error_plots_over_time.{fig_type}')

    error_distribution_over_time(df_diff, diff_columns, output_file=args.output + f'/date_boxplots.{fig_type}')

    d = df_diff.melt(id_vars=['Time'], value_vars=diff_columns).dropna()
    d['Year'] = d['Time'].dt.hour
    d['Day'] = d['Time'].dt.minute
    d['Time'] = pd.to_datetime(d['Day'].apply(str) + '/' + d['Year'].apply(str), format='%M/%H')
    sns.lineplot(x='Time', y='value', data=d)

    plt.savefig(args.output + f'/day_of_week_error.{fig_type}')
    plt.clf()

    d = df_diff.melt(id_vars=['Time'], value_vars=diff_columns).dropna()
    d['Day'] = d['Time'].dt.day_name()
    sns.boxplot(y='Day', x='value', data=d)

    plt.savefig(args.output + f'/day_of_week_boxplot.{fig_type}')
    plt.clf()

    columns = sorted(list(set(columns_pred).intersection(set(columns_actual))))
    diff_data = []
    for c in columns:
        diff = list(abs(df[f'{c}_pred'] - df[f'{c}_actual']))
        pred = list(df[f'{c}_pred'])
        actual = list(df[f'{c}_actual'])
        times = list(df['Time'])
        diff_data.extend(zip(diff, pred, actual, times, [c] * len(times)))

    def actual_vs_predicted_distribution(diff_data, hour, minute, station):
        v1 = [a for (e, p, a, t, r) in diff_data if r == station and t.hour == hour and t.minute == minute]
        v2 = [p for (e, p, a, t, r) in diff_data if r == station and t.hour == hour and t.minute == minute]
        try:
            if len(v1) > 0 and len(v2) > 0:
                min_v = min(min(v1), min(v2))
                max_v = max(max(v1), max(v2))
                plt.hist(v1, int(max_v - min_v), range=(min_v, max_v), alpha=0.5, label='actual')
                plt.hist(v2, int(max_v - min_v), range=(min_v, max_v), alpha=0.5, label='predicted')
                plt.legend()
                pathlib.Path(args.output + '/actual_vs_predicted_distribution/').mkdir(parents=True, exist_ok=True)
                plt.savefig(args.output + f'/actual_vs_predicted_distribution/{hour}_{minute}_{station}.{fig_type}')
                plt.clf()
        except Exception as ex:
            print(ex)

    # actual_vs_predicted_distribution(diff_data, 5, 45, 'SUM(1006-22a:Smer 1: AC priključek > Ulica bratov Komel)')
    # actual_vs_predicted_distribution(diff_data, 7, 30, 'SUM(1006-22a:Smer 1: AC priključek > Ulica bratov Komel)')

    actual_vs_predicted_series(diff_data, 7, 30, 'SUM(1006-22a:Smer 1: AC priključek > Ulica bratov Komel)',
                               output_dir=args.output + '/actual_vs_predicted_series/', fig_type=fig_type)
    actual_vs_predicted_series(diff_data, 5, 45, 'SUM(1006-22a:Smer 1: AC priključek > Ulica bratov Komel)',
                               output_dir=args.output + '/actual_vs_predicted_series/', fig_type=fig_type)
