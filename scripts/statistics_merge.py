import argparse

import yaml

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--statistics', nargs='+', required=True)
    parser.add_argument('-o', '--output', type=str, required=True)
    args = parser.parse_args()

    yaml_list = []
    for file in args.statistics:
        with open(file, 'r') as yaml_file:
            d = yaml.load(yaml_file, Loader=yaml.FullLoader)
            yaml_list.append(d)

    with open(args.output, 'w') as outfile:
        yaml.dump(yaml_list, outfile, default_flow_style=False)
