import argparse
import pandas as pd
from moltraffic.visualisations import grouped_mean_data_plot, grouped_mean_minute_data_over_day


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Graph daily use.')
    parser.add_argument('-i', '--input', type=str, required=True)
    parser.add_argument('-o', '--output', type=str, required=True)
    args = parser.parse_args()

    df = pd.read_csv(args.input)
    grouped_mean_data_plot(df, args.output, grouping_function=grouped_mean_minute_data_over_day)
