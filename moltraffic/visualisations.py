import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import pathlib


def get_vehicle_types():
    return ['Mot', 'Osebni', 'BUS', 'LTov', 'STov', 'TTov', 'TSP', 'Vlac']


def grouped_mean_minute_data_over_day(df):
    """
    Group by hours and minute and compute mean number of vehicles
    """
    df['Time'] = pd.to_datetime(df['Time'])
    df['Hour'] = df['Time'].dt.hour
    df['Minute'] = df['Time'].dt.minute
    gdf = df.groupby(['Hour', 'Minute']).agg({x: 'mean' for x in get_vehicle_types()}).reset_index()
    gdf['Time'] = pd.to_datetime(gdf['Hour'].apply(str) + ':' + gdf['Minute'].apply(str), format='%H:%M')
    return gdf


def plot_mean_hourly_vehicles(df):
    """
    Group by month and compute mean number of vehicles
    """
    df['Time'] = pd.to_datetime(df['Time'])
    df['Month'] = df['Time'].dt.month
    gdf = df.groupby(['Month']).agg({x: 'mean' for x in get_vehicle_types()}).reset_index()
    gdf['Time'] = pd.to_datetime(gdf['Month'].apply(str), format='%m')
    return gdf


def grouped_mean_month_data_over_years(df):
    """
    Group by year and month and compute mean number of vehicles
    """
    df['Time'] = pd.to_datetime(df['Time'])
    df['Year'] = df['Time'].dt.year
    df['Month'] = df['Time'].dt.month
    gdf = df.groupby(['Year', 'Month']).agg({x: 'mean' for x in get_vehicle_types()}).reset_index()
    gdf['Time'] = pd.to_datetime(gdf['Month'].apply(str) + '/' + gdf['Year'].apply(str), format='%m/%Y')
    return gdf


def grouped_mean_data_plot(df, output_file, grouping_function):
    """
    Plot means for different vehicle types
    """
    vehicle_types = get_vehicle_types()
    gdf = grouping_function(df)

    ax = None
    for vehicle_type in vehicle_types:
        if ax is None:
            ax = gdf.plot(x='Time', y=vehicle_type)
        else:
            gdf.plot(x='Time', y=vehicle_type, ax=ax)
    plt.savefig(output_file, bbox_inches='tight', pad_inches=0)


def station_error_boxplot(df, output_file=None):
    d = df.melt().dropna()
    sns.set(rc={'figure.figsize': (15, int(len(d['variable'].unique()) * 0.5))})
    sns.boxplot(x='value', y='variable', data=d)
    if output_file is None:
        plt.show()
    else:
        plt.savefig(output_file, bbox_inches='tight', pad_inches=0)
    plt.clf()


def error_distribution(df, output_file=None):
    d = df.melt().dropna()
    sns.histplot(data=d, x="value")
    if output_file is None:
        plt.show()
    else:
        plt.savefig(output_file, bbox_inches='tight', pad_inches=0)
    plt.clf()


def mean_error_over_time(df_diff, diff_columns, output_file=None):
    d = df_diff.melt(id_vars=['Time'], value_vars=diff_columns).dropna()
    d['Year'] = d['Time'].dt.year
    d['Day'] = d['Time'].dt.dayofyear
    d['Time'] = pd.to_datetime(d['Day'].apply(str) + '/' + d['Year'].apply(str), format='%j/%Y')
    sns.lineplot(x='Time', y='value', data=d)
    if output_file is None:
        plt.show()
    else:
        plt.savefig(output_file, bbox_inches='tight', pad_inches=0)
    plt.clf()


def all_errors_over_time(df_diff, diff_columns, output_file=None):
    dm = df_diff.melt(id_vars=['Time'], value_vars=diff_columns).dropna()
    dm['Year'] = dm['Time'].dt.year
    dm['Day'] = dm['Time'].dt.dayofyear
    dm['Time'] = pd.to_datetime(dm['Day'].apply(str) + '/' + dm['Year'].apply(str), format='%j/%Y')
    for diff_column in diff_columns:
        d = dm[dm.variable == diff_column].copy()
        sns.lineplot(x='Time', y='value', data=d, ci=None)
    if output_file is None:
        plt.show()
    else:
        plt.savefig(output_file, bbox_inches='tight', pad_inches=0)
    plt.clf()


def error_distribution_over_time(df_diff, diff_columns, output_file=None):
    d = df_diff.melt(id_vars=['Time'], value_vars=diff_columns).dropna()
    d['Year'] = d['Time'].dt.year
    d['Day'] = d['Time'].dt.dayofyear
    d['Time'] = pd.to_datetime(d['Day'].apply(str) + '/' + d['Year'].apply(str), format='%j/%Y')
    sns.boxplot(y='Time', x='value', data=d)
    sns.set(rc={'figure.figsize': (15, int(len(d['Time'].unique()) * 0.2))})
    if output_file is None:
        plt.show()
    else:
        plt.savefig(output_file, bbox_inches='tight', pad_inches=0)
    plt.clf()


def actual_vs_predicted_series(diff_data, hour, minute, station, output_dir=None, fig_type='pdf', fig_size=None):
    v1 = sorted([(t, a) for (e, p, a, t, r) in diff_data if r == station and t.hour == hour and t.minute == minute])
    v2 = sorted([(t, p) for (e, p, a, t, r) in diff_data if r == station and t.hour == hour and t.minute == minute])
    number_of_times = len([x[0] for x in v1])
    if fig_size is None:
        sns.set(rc={'figure.figsize': (int(number_of_times/10), 10)})
    else:
        sns.set(rc={'figure.figsize': fig_size})
    plt.plot([x[0] for x in v1], [x[1] for x in v1], label='actual')
    plt.plot([x[0] for x in v2], [x[1] for x in v2], label='predicted')
    plt.legend()
    if output_dir is None:
        plt.show()
    else:
        pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
        plt.savefig(output_dir + f'/{hour}_{minute}_{station}.{fig_type}', bbox_inches='tight', pad_inches=0)
    plt.clf()
