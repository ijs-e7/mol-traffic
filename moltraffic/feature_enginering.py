import math
import warnings
import numpy as np
import pandas as pd
import pytz


def one_hot_months(df):
    new_features = []
    for i in range(1, 13):
        m = (df['Time'].dt.month == i).astype(int).rename(f'Month_{i}')
        new_features.append(m)
    return new_features


def one_hot_hour(df):
    new_features = []
    for i in range(24):
        m = (df['Time'].dt.hour == i).astype(int).rename(f'Hour_{i}')
        new_features.append(m)
    return new_features


def one_hot_minute(df):
    new_features = []
    for i in [0, 15, 30, 45]:
        m = (df['Time'].dt.minute == i).astype(int).rename(f'Minute_{i}')
        new_features.append(m)
    return new_features


def one_hot_day_of_the_year(df):
    new_features = []
    for i in range(1, 367):
        m = (df['Time'].dt.day_of_year == i).astype(int).rename(f'Day_of_year_{i}')
        new_features.append(m)
    return new_features


def one_hot_day_of_the_week(df):
    new_features = []
    days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
    for i, name in zip(range(7), days):
        m = (df['Time'].dt.dayofweek == i).astype(int).rename(f'is_{name}')
        new_features.append(m)
    return new_features


def sin_cos_hour_min(df):
    new_features = []
    max_t = 24 * 60
    m = (df['Time'].dt.hour.astype(int) * 60 + df['Time'].dt.minute.astype(int)).to_list()
    m2 = np.sin([2 * math.pi * x / max_t for x in m])
    m3 = np.cos([2 * math.pi * x / max_t for x in m])
    new_features.append(pd.Series(m2).rename('SinTime'))
    new_features.append(pd.Series(m3).rename('CosTime'))
    return new_features


def sin_cos_day_hour_min(df):
    new_features = []
    max_t = 24 * 60 * 7
    m = (df['Time'].dt.dayofweek * 24 * 60 + df['Time'].dt.hour * 60 + df['Time'].dt.minute).to_list()
    m2 = np.sin([2 * math.pi * x / max_t for x in m])
    m3 = np.cos([2 * math.pi * x / max_t for x in m])
    new_features.append(pd.Series(m2).rename('SinWeekTime'))
    new_features.append(pd.Series(m3).rename('CosWeekTime'))
    return new_features


def sin_cos_day_of_year(df):
    new_features = []
    max_t = 366
    m = (df['Time'].dt.day_of_year.astype(int)).to_list()
    m2 = np.sin([2 * math.pi * x / max_t for x in m])
    m3 = np.cos([2 * math.pi * x / max_t for x in m])
    new_features.append(pd.Series(m2).rename('SinDayOfYear'))
    new_features.append(pd.Series(m3).rename('CosDayOfYear'))
    return new_features


def weather_data(weather_df, df):
    new_features = []
    weather_numerical_columns = ['povp. dnevna T [°C]', 'povp. veter [m/s]', 'oblačnost [%]', 'povp. rel. vla. [%]',
                                 'povp. tlak [hPa]', 'količina padavin [mm]', 'višina snežne odeje [cm]',
                                 'trajanje sonca [h]']
    # weather_binary_columns = ['dež', 'rosenje', 'sneg', 'megla', 'poledica', 'poledica na tleh', 'padavine',
    #                          'snežna odeja']

    weather_binary_columns = ['rosenje', 'poledica', 'poledica na tleh']

    for i in [0, -1, -2, -3, -4, -5]:
        merge_column_name = 'valid_str_' + str(i)
        df[merge_column_name] = (df['Time'] + pd.Timedelta(days=i)).dt.strftime('%Y-%m-%d')
        weather_df[merge_column_name] = weather_df['valid'].dt.strftime('%Y-%m-%d')
        weather_df_merged = pd.merge(df, weather_df, on=merge_column_name, how='left')
        for c in weather_numerical_columns:
            m = weather_df_merged[c].astype(float).rename(c + '_' + str(i)).fillna(0)
            new_features.append(m)
        for c in weather_binary_columns:
            m = weather_df_merged[c].astype(str).map(lambda x: int(x == 'da')).rename(c + '_' + str(i)).fillna(0)
            new_features.append(m)
        df.drop(columns=[merge_column_name], inplace=True)
    return new_features


def holiday_data(holiday_df, df):
    from functools import partial
    new_features = []
    d = dict()
    cl = [x for x in holiday_df.columns if x.lower() != 'date']
    for index, row in holiday_df.iterrows():
        date, stay_home = row['date'], row[cl]
        day, month, year = date.split('.')
        if 'x' in day.lower() and 'x' in month.lower() and 'x' in year.lower():
            d['def'] = stay_home
        elif 'x' in year.lower():
            d[(day, month)] = stay_home
        else:
            d[(day, month, year)] = stay_home

    def app(v, holiday_map):
        day, month, year = v.split('.')
        if (day, month, year) in holiday_map:
            return holiday_map[(day, month, year)]
        elif (day, month) in holiday_map:
            return holiday_map[(day, month)]
        else:
            return holiday_map['def']

    f = partial(app, holiday_map=d)
    new_df = df['Time'].dt.strftime('%d.%m.%Y').apply(f)
    for col in new_df.columns:
        new_features.append(new_df[col])

    return new_features


def is_dst(dt, timezone="UTC"):
    try:
        timezone = pytz.timezone(timezone)
        timezone_aware_date = timezone.localize(dt, is_dst=None)
        return timezone_aware_date.tzinfo._dst.seconds != 0
    except Exception as e:
        warnings.warn(f"Dates {e} are assigned to DST.")
        return False


def is_dst_feature(df):
    new_features = []
    m = df['Time'].apply(lambda x: int(is_dst(x, timezone="Europe/Ljubljana"))).astype(int).rename('dst')
    new_features.append(m)
    return new_features


def daylight_savings_time_transform(df):
    v = df.copy()
    v['tmp'] = v['Time'].apply(lambda x: is_dst(x, timezone="Europe/Ljubljana"))
    v.loc[v['tmp'], 'Time'] = v[v['tmp']]['Time'] - pd.Timedelta(hours=1)
    v.drop(columns=['tmp'])
    return v


def feature_creation(df, weather_df=None, holiday_df=None):
    new_features = []
    if weather_df is None:
        warnings.warn('No weather features will be created')
    else:
        new_features.extend(weather_data(weather_df, df))
    if holiday_df is None:
        warnings.warn('No holidays features will be created')
    else:
        new_features.extend(holiday_data(holiday_df, df))
    new_features.extend(one_hot_months(df))
    new_features.extend(one_hot_day_of_the_week(df))
    new_features.extend(one_hot_day_of_the_year(df))
    new_features.extend(one_hot_hour(df))
    new_features.extend(one_hot_minute(df))
    new_features.extend(sin_cos_hour_min(df))
    new_features.extend(sin_cos_day_hour_min(df))
    new_features.extend(sin_cos_day_of_year(df))
    new_features.extend(linear_timespan_feture(df))
    new_features.extend(is_dst_feature(df))
    concat = pd.concat(new_features, axis=1)
    return concat


def linear_timespan_feture(df):
    new_features = []
    min_time = pd.to_datetime("1/1/2013")
    max_time = pd.to_datetime("1/1/2021")

    max_diff = (max_time - min_time).total_seconds()
    date_diff = (df['Time']-min_time).dt.total_seconds()
    m = date_diff / max_diff

    new_features.append(m)
    return new_features
