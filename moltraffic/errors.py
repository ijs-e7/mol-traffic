from collections import defaultdict
import numpy as np
from moltraffic.data_transformations import DATESET_COLUMNS
import math
from abc import ABC, abstractmethod


class ErrorCalculation(ABC):
    @abstractmethod
    def get_errors(self):
        pass

    @abstractmethod
    def get_errors_distribution(self):
        pass


class MeanAbsoluteError(ErrorCalculation, ABC):
    def __init__(self, Y, Y_, column_names=None):
        self.Y = Y
        self.Y_ = Y_
        self.nans = np.invert(np.isnan(self.Y))
        self.diff = np.abs((self.Y_ - self.Y)[self.nans]).flatten()

    def get_errors(self):
        mae = float(np.mean(self.diff))
        return [('MAE', mae)]

    def get_errors_distribution(self):
        return [('MAE', self.diff)]


class MeanSquaredError(ErrorCalculation):
    def __init__(self, Y, Y_, column_names=None):
        self.Y = Y
        self.Y_ = Y_
        self.nans = np.invert(np.isnan(self.Y))
        self.diff = np.square((self.Y_ - self.Y)[self.nans]).flatten()

    def get_errors(self):
        mse = float(np.mean(self.diff))
        return [('MSE', mse)]

    def get_errors_distribution(self):
        return [('MSE', self.diff)]


class MeanAbsoluteErrorIndividualVehicleSumPredictions(ErrorCalculation):
    def __init__(self, Y, Y_, column_names=None):
        self.Y = Y
        self.Y_ = Y_
        self.column_names = column_names
        self.stations = set()
        self.types = set()
        for x in self.column_names:
            self.types.add(x.split('(')[0])
        for x in self.column_names:
            xt = x
            for t in self.types:
                xt = xt.replace(t, '')
            self.stations.add(xt)

    def get_errors(self):
        all_sum = 0
        all_count = 0
        for station in self.stations:
            indexes = []
            for typ in self.types:
                index = self.column_names.index(typ + station)
                indexes.append(index)

            if len(indexes) == 0:
                continue

            sum_pred = np.sum(self.Y_[:, indexes], axis=1)
            sum_actual = np.sum(self.Y[:, indexes], axis=1)

            diff = np.abs(sum_actual - sum_pred)
            nans = np.invert(np.isnan(diff))
            selected = diff[nans]
            all_sum += np.sum(selected)
            all_count += np.prod(selected.shape)
        if all_count == 0:
            return [('MSE-sum-types', math.nan)]
        return [('MSE-sum-types', float(all_sum / all_count))]

    def get_errors_distribution(self):
        return []


class MeanAbsoluteErrorIndividualVehiclePredictions(ErrorCalculation):
    def __init__(self, Y, Y_, column_names=None):
        self.Y = Y
        self.Y_ = Y_
        self.column_names = column_names
        self.stations = []
        for x in self.column_names:
            xt = x
            for t in DATESET_COLUMNS:
                xt = xt.replace(t, '')
            self.stations.append(xt)

    def get_errors(self):
        diff = (self.Y_ - self.Y)
        type_sum = defaultdict(lambda: 0)
        type_count = defaultdict(lambda: 0)
        for i, c in enumerate(self.column_names):
            for typ in DATESET_COLUMNS:
                if typ in c:
                    df = diff[:, i].flatten()
                    nans = np.invert(np.isnan(df))
                    selected = df[nans].flatten()
                    type_sum[typ] += np.sum(np.abs(selected))
                    type_count[typ] += np.prod(selected.shape)
        return_metrics = []
        for typ in DATESET_COLUMNS:
            if typ in type_count.keys():
                return_metrics.append(('MSEiv-' + typ, float(type_sum[typ] / type_count[typ])))
            else:
                return_metrics.append(('MSEiv-' + typ, float(math.nan)))
        return return_metrics

    def get_errors_distribution(self):
        return []
