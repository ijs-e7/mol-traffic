import pandas as pd
import numpy as np
import warnings

DATESET_COLUMNS = ['Mot', 'Osebni', 'BUS', 'LTov', 'STov', 'TTov', 'TSP', 'Vlac']


def add_full_name_column(df):
    df['FULL'] = df['SRC'] + ':' + df['STM'] + '-' + df['NAME'] + '-' + df['DIR']
    df['FULL'] = df['FULL'].str.replace(' ', '')
    return df


def drop_nan_STM(df):
    return df[df['newSTM'].notna()]


def transform_times(df):
    df['Time'] = pd.to_datetime(df['Time'])
    return df


def sum_vehicles(df):
    df['SUM'] = df[DATESET_COLUMNS].sum(axis=1)
    df = df.drop(columns=DATESET_COLUMNS)
    return df


def unique_station_name(df):
    df['ID'] = df['STM'] + ':' + df['DIR']
    return df


DATA_TRANSFORMATION_FUNCTIONS = dict()


def register_data_loading_function(func):
    """Register a function as a plug-in"""
    DATA_TRANSFORMATION_FUNCTIONS[func.__name__.lower()] = func
    return func


def get_data_loading_function(name):
    return DATA_TRANSFORMATION_FUNCTIONS[name]


def all_data_loading_function():
    return list(DATA_TRANSFORMATION_FUNCTIONS.keys())


@register_data_loading_function
def load_and_sum_vehicle_types_dataset(file):
    df = (
        pd.read_csv(file)
        .pipe(transform_times)
        .drop(columns=['Err', 'OCC', 'GAP', 'VMIN', 'VAVG', 'VMAX', 'SRC', 'NAME'])
        .pipe(sum_vehicles)
        .pipe(unique_station_name)
        .drop(columns=['STM', 'DIR', 'FULL'])
    )

    Y_columns = sorted(list(set(df['ID'].to_list())))

    pdf = (
        df.pivot_table(index='Time', columns='ID', values='SUM', aggfunc='mean')
        .reset_index()
    )

    X_pdf, Y_pdf = pdf[['Time']], pdf[Y_columns]
    Y_pdf = Y_pdf.rename(columns={x: f'SUM({x})' for x in Y_pdf.columns})
    return X_pdf, Y_pdf


def all_vehicles(df):
    return df


@register_data_loading_function
def load_individual_vehicle_types_dataset(file):
    df = (
        pd.read_csv(file)
        .pipe(transform_times)
        .drop(columns=['Err', 'OCC', 'GAP', 'VMIN', 'VAVG', 'VMAX', 'SRC', 'NAME'])
        .pipe(all_vehicles)
        .pipe(unique_station_name)
        .drop(columns=['STM', 'DIR', 'FULL'])
    )

    Y_columns = sorted(list(set(df['ID'].to_list())))

    pdf = (
        df.pivot_table(index='Time', columns='ID', values=DATESET_COLUMNS, aggfunc='mean')
        .reset_index()
    )

    Y_columns_2 = []
    for c1 in DATESET_COLUMNS:
        for c2 in Y_columns:
            Y_columns_2.append((c1, c2))

    X_pdf, Y_pdf = pdf[['Time']], pdf[Y_columns_2]
    X_pdf.columns = [x[0] for x in X_pdf.columns]
    Y_pdf.columns = [f'{x[0]}({x[1]})' for x in Y_pdf.columns]
    return X_pdf, Y_pdf


def car_vehicles(df):
    df['Car'] = df['Osebni']
    df = df.drop(columns=DATESET_COLUMNS)
    return df


@register_data_loading_function
def load_cars_vehicle_types_dataset(file):
    df = (
        pd.read_csv(file)
        .pipe(transform_times)
        .drop(columns=['Err', 'OCC', 'GAP', 'VMIN', 'VAVG', 'VMAX', 'SRC', 'NAME'])
        .pipe(car_vehicles)
        .pipe(unique_station_name)
        .drop(columns=['STM', 'DIR', 'FULL'])
    )

    Y_columns = sorted(list(set(df['ID'].to_list())))

    pdf = (
        df.pivot_table(index='Time', columns='ID', values='Car', aggfunc='mean')
        .reset_index()
    )

    X_pdf, Y_pdf = pdf[['Time']], pdf[Y_columns]
    Y_pdf = Y_pdf.rename(columns={x: f'Car({x})' for x in Y_pdf.columns})
    return X_pdf, Y_pdf


def load_weather_data(file):
    if file is None:
        warnings.warn('No weather file loaded')
        return None
    weather_pdf = pd.read_csv(file)
    weather_pdf.rename(columns=lambda x: x.strip(), inplace=True)
    weather_pdf['valid'] = pd.to_datetime(weather_pdf['valid'], format='%Y-%m-%d')
    return weather_pdf


def load_holiday_data(file):
    if file is None:
        warnings.warn('No holidays file loaded')
        return None
    return pd.read_csv(file)


def keep_columns_that_were_in_train_zero_rest(Y_columns, pdf):
    c = []
    for cn in Y_columns:
        if cn in pdf.columns:
            m = np.asmatrix(pdf[cn].to_list()).T
            c.append(m)
        else:
            m = np.asmatrix([np.nan] * len(pdf)).T
            warnings.warn(f'Column {cn} not in test set. It will be set to nan.')
            c.append(m)
    Y = np.hstack(c)
    for cn in pdf.columns:
        if cn not in Y_columns:
            warnings.warn(f'Column {cn} from test test will be ignored since it was not in train set.')
    return Y
