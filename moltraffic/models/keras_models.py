import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow.python.keras.layers import Dense
from tensorflow.python.keras.models import Sequential


def get_keras_net(input_shape, output_shape):
    model = Sequential()
    model.add(Dense(128, activation='relu', input_dim=input_shape))
    model.add(Dense(128, activation='relu'))
    model.add(Dense(output_shape, activation='relu'))
    return model


def neg_log_likelihood(y, rv_y):
    return -rv_y.log_prob(y)


def get_keras_distribution_net(input_shape, output_shape):
    model = tf.keras.Sequential([
        tf.keras.layers.Dense(128, activation='relu', input_dim=input_shape),
        tf.keras.layers.Dense(128, activation='relu'),
        tf.keras.layers.Dense(output_shape + output_shape, activation='linear'),
        tfp.layers.DistributionLambda(
            lambda t: tfp.distributions.Normal(loc=tf.math.softplus(t[..., :output_shape]),
                                               scale=1e-3 + tf.math.softplus(0.05 * t[..., output_shape:]))),
    ])
    return model
