import torch
from torch.utils.data import Dataset
import pytorch_lightning as pl
from torch import nn
import torch.nn.functional as F


class PytorchDataset(Dataset):
    def __init__(self, X, Y):
        self.size = X.shape[0]
        self.X = torch.from_numpy(X).float()
        self.Y = torch.from_numpy(Y).float()

    def __len__(self):
        return self.size

    def __getitem__(self, index):
        return self.X[index], self.Y[index]


def l1_missing_values_loss(output, target):
    mask = torch.isnan(target) == 0
    loss = torch.mean(torch.abs(output[mask] - target[mask]))
    return loss


class PytorchNetGeneral(pl.LightningModule):
    def __init__(self, input_size, output_size, hidden_layer_size=128, number_of_hidden_layers=2, use_dropout=True):
        super().__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.hidden_size = hidden_layer_size
        self.use_dropout = use_dropout

        self.layers_sizes = [input_size] + (number_of_hidden_layers-1)*[hidden_layer_size] + [output_size]
        layer_size_tuples = list(zip(self.layers_sizes, self.layers_sizes[1:]))
        self.linear_layers = nn.ModuleList([nn.Linear(input, output) for input, output in layer_size_tuples])
        self.dropout_layers = nn.ModuleList(nn.Dropout(p=0.2) for _ in range(len(self.linear_layers)-1))

    def forward(self, x):
        for linear_l, dropout_l in zip(self.linear_layers[:-1], self.dropout_layers):
            x = F.relu(linear_l(x))
            if self.use_dropout:
                x = dropout_l(x)
        x = torch.abs(self.linear_layers[-1](x))
        return x

    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = l1_missing_values_loss(y_hat, y)
        return loss

    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = l1_missing_values_loss(y_hat, y)
        self.log('validation_loss', loss, on_epoch=True, reduce_fx=torch.median)

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters())
        return optimizer
    
    
class PytorchLinearRegression(pl.LightningModule):
    def __init__(self, input_size, output_size):
        super().__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.linear = torch.nn.Linear(self.input_size, self.output_size)

    def forward(self, x):
        out = self.linear(x)
        return out

    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = l1_missing_values_loss(y_hat, y)
        return loss

    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = l1_missing_values_loss(y_hat, y)
        self.log('validation_loss', loss, on_epoch=True, reduce_fx=torch.median)

    def configure_optimizers(self):
        optimizer = torch.optim.SGD(self.parameters(), lr=0.01)
        return optimizer
