import os
import pickle
from abc import ABC
from abc import abstractmethod
import numpy as np
import pytorch_lightning as pl
import tensorflow as tf
import torch
from sklearn.dummy import DummyRegressor
from sklearn.linear_model import ElasticNet, SGDRegressor
from sklearn.multioutput import MultiOutputRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor, ExtraTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from tensorflow.python.keras.models import load_model
from torch.utils.data import DataLoader
from xgboost import XGBRegressor
from moltraffic.models.keras_models import get_keras_distribution_net, get_keras_net, neg_log_likelihood
from moltraffic.models.pytorch_models import PytorchDataset, l1_missing_values_loss, PytorchNetGeneral, PytorchLinearRegression
import copy
from pytorch_lightning.callbacks import Callback, EarlyStopping
from pathlib import Path


class MOLModel(ABC):
    _registry = {}

    def __init_subclass__(cls, is_registered=True, **kwargs):
        super().__init_subclass__(**kwargs)
        if is_registered:
            MOLModel._registry[cls.__name__] = cls

    @staticmethod
    def get_registered_models():
        return MOLModel._registry

    @staticmethod
    def get_registered_model(name):
        return MOLModel._registry[name]

    @abstractmethod
    def predict(self, X):
        pass

    @abstractmethod
    def fit(self, X, Y, threads=1):
        pass

    @abstractmethod
    def save(self, path):
        pass


class SkLearnModel(MOLModel, ABC, is_registered=False):
    @staticmethod
    def load_model(directory):
        return pickle.load(open(directory + '/model.p', "rb"))

    def __init__(self, model):
        self.model = model

    def predict(self, X):
        return self.model.predict(X)

    def fit(self, X, Y, threads=1):
        Y = np.nan_to_num(Y)
        self.model.fit(X, Y)

    def save(self, directory):
        pickle.dump(self, open(directory + '/model.p', "wb"))


class ElasticNetModel(SkLearnModel):
    def __init__(self):
        model = Pipeline([('MinMaxScaler', MinMaxScaler()), ('ElasticNet', ElasticNet())])
        super().__init__(model)


class DummyRegressorModel(SkLearnModel):
    def __init__(self):
        super().__init__(DummyRegressor())


class MLPRegressorModel(SkLearnModel):
    def __init__(self):
        model = MLPRegressor()
        super().__init__(model)


class DecisionTreeRegressorModel(SkLearnModel):
    def __init__(self):
        super().__init__(DecisionTreeRegressor(min_samples_leaf=4, criterion='mae'))
        
        
    
class DecisionTreeDepth2RegressorModel(SkLearnModel):
    def __init__(self):
        super().__init__(DecisionTreeRegressor(max_depth=2, criterion='mae'))
        
class DecisionTreeDepth3RegressorModel(SkLearnModel):
    def __init__(self):
        super().__init__(DecisionTreeRegressor(max_depth=3, criterion='mae'))
        
class DecisionTreeDepth4RegressorModel(SkLearnModel):
    def __init__(self):
        super().__init__(DecisionTreeRegressor(max_depth=4, criterion='mae'))
        
class DecisionTreeDepth5RegressorModel(SkLearnModel):
    def __init__(self):
        super().__init__(DecisionTreeRegressor(max_depth=5, criterion='mae'))
        
class DecisionTreeDepth6RegressorModel(SkLearnModel):
    def __init__(self):
        super().__init__(DecisionTreeRegressor(max_depth=6, criterion='mae'))
        
class DecisionTreeDepth7RegressorModel(SkLearnModel):
    def __init__(self):
        super().__init__(DecisionTreeRegressor(max_depth=7, criterion='mae'))
        
class DecisionTreeDepth8RegressorModel(SkLearnModel):
    def __init__(self):
        super().__init__(DecisionTreeRegressor(max_depth=8, criterion='mae'))
        
        
        


class ExtraTreeRegressorModel(SkLearnModel):
    def __init__(self):
        super().__init__(ExtraTreeRegressor(min_samples_leaf=4, criterion='mae'))
        

class RandomForestRegressorModel(SkLearnModel, is_registered=False):
    def __init__(self):
        super().__init__(RandomForestRegressor(min_samples_leaf=4, criterion='mae'))


class SVRModel(SkLearnModel, is_registered=False):
    def __init__(self):
        super().__init__(MultiOutputRegressor(SVR()))
        

class MAELinearRegression(SkLearnModel):
    def __init__(self):
        super().__init__(MultiOutputRegressor(SGDRegressor(loss='epsilon_insensitive', epsilon=0)))


class MLPRegressorSingleModel(MOLModel, is_registered=False):
    @staticmethod
    def load_model(directory):
        i = 0
        return_model = MLPRegressorSingleModel()
        while os.path.isfile(directory + f'/model_{i}.p'):
            return_model.models.append(pickle.load(open(directory + f'/model_{i}.p', "rb")))
            i += 1
        return return_model

    def __init__(self):
        self.model_type = MLPRegressor
        self.models = []

    def predict(self, X):
        X_list = []
        for model in self.models:
            p = model.predict(X)
            X_list.append(p)
        s = np.vstack(X_list).T
        return s

    def train_model(self, X, y):
        model = self.model_type()
        model.fit(X, y)
        return model

    def fit(self, X, Y, threads=1):
        Y = np.nan_to_num(Y)
        self.models = [self.train_model(X, Y[:, i]) for i in range(Y.shape[1])]

    def save(self, directory):
        for i, model in enumerate(self.models):
            pickle.dump(model, open(directory + f'/model_{i}.p', "wb"))


def get_class_name(o):
    klass = o.__class__
    module = klass.__module__
    if module == 'builtins':
        # Avoid outputs like 'builtins.str'
        return klass.__qualname__
    return module + '.' + klass.__qualname__


def import_from_class_name(name):
    components = name.split('.')
    mod = __import__(components[0])
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod


class MetricsCallback(Callback):
    """PyTorch Lightning metric callback."""

    def __init__(self):
        super().__init__()
        from collections import defaultdict
        self.metrics = defaultdict(lambda: [])

    def on_validation_end(self, trainer, pl_module):
        each_me = copy.deepcopy(trainer.callback_metrics)
        for k, i in each_me.items():
            self.metrics[k].append(i.item())


class MyPrintingCallback(Callback):
    def __init__(self, data):
        self.data = data
        self.loss = []

    def on_train_epoch_end(self, trainer, pl_module):
        error = 0
        dataloader = DataLoader(self.data, batch_size=64)
        for i_batch, sample_batched in enumerate(dataloader):
            x, y = sample_batched
            pl_module.eval()
            y_hat = pl_module(x)
            loss = l1_missing_values_loss(y_hat, y).item()
            error += loss
            pl_module.train()
        self.loss.append(error/len(dataloader))


class PytorchMultitaskModel(MOLModel, ABC, is_registered=False):
    def __init__(self, pytorch_model_types, pytorch_model_arguments=None):
        super().__init__()
        assert isinstance(pytorch_model_types, list)
        assert len(pytorch_model_types) > 0

        self.pytorch_model_types = pytorch_model_types
        if pytorch_model_arguments is None:
            pytorch_model_arguments = {}
        self.pytorch_model_arguments = pytorch_model_arguments
        self.Y_scale_transformer = None
        self.X_scale_transformer = None
        self.models = []
        self.epochs = 200
        self.batch_size = 256
        self.input_size = None
        self.output_size = None

    def predict(self, X):
        X = self.X_scale_transformer.transform(X)
        Xt = torch.from_numpy(X).float()
        preds = [self.Y_scale_transformer.inverse_transform(model(Xt).detach().numpy()) for model in self.models]
        Y = np.zeros_like(preds[0])
        for p in preds:
            Y += p / len(self.models)
        return Y

    def save(self, directory):
        for i, model in enumerate(self.models):
            torch.save(model.state_dict(), directory + f'/state_dict_model_{i}.pt')
        params = {'input_size': self.input_size, 'output_size': self.output_size, **self.pytorch_model_arguments}
        meta = {'model_model_type': get_class_name(self),
                'pytorch_model_type': [get_class_name(x) for x in self.models],
                'params': params}
        pickle.dump(meta, open(directory + "/meta.p", "wb"))
        self.save_transformers(directory)

    def save_transformers(self, directory):
        pickle.dump(self.Y_scale_transformer, open(directory + "/scala_transformet.p", "wb"))
        pickle.dump(self.X_scale_transformer, open(directory + "/X_scala_transformet.p", "wb"))

    @staticmethod
    def load_transformers(directory):
        Y_tr = pickle.load(open(directory + "/scala_transformet.p", "rb"))
        X_tr = pickle.load(open(directory + "/X_scala_transformet.p", "rb"))
        return X_tr, Y_tr

    @staticmethod
    def load_models(directory, meta):
        models = []
        for i, model_type in enumerate(meta['pytorch_model_type']):
            model = import_from_class_name(model_type)(**meta['params'])
            model.load_state_dict(torch.load(directory + f'/state_dict_model_{i}.pt'))
            models.append(model)
        return models

    def individual_model_train(self, model, X=None, Y=None):
        data = PytorchDataset(X, Y)
        validation_size = int(len(data) * 0.1)
        train_size = len(data) - validation_size
        train_data, validation_data = torch.utils.data.random_split(data, [train_size, validation_size])
        train_dataloader = DataLoader(train_data, batch_size=self.batch_size, shuffle=True, drop_last=True)
        val_dataloader = DataLoader(validation_data, batch_size=self.batch_size, shuffle=True, drop_last=True)

        val_callback = MetricsCallback()
        train_callback = MyPrintingCallback(train_data)
        stopping_callback = EarlyStopping(monitor='validation_loss', patience=5)
        trainer = pl.Trainer(max_epochs=self.epochs,
                             progress_bar_refresh_rate=0,
                             callbacks=[val_callback, train_callback, stopping_callback])

        trainer.fit(model, train_dataloader=train_dataloader, val_dataloaders=val_dataloader)
        val_loss = val_callback.metrics['validation_loss'][1:]
        train_loss = train_callback.loss
        return model, val_loss, train_loss

    def data_transform(self, X, Y):
        self.Y_scale_transformer = MinMaxScaler()
        self.X_scale_transformer = MinMaxScaler()
        Y = self.Y_scale_transformer.fit_transform(Y)
        X = self.X_scale_transformer.fit_transform(X)
        return X, Y

    def fit(self, X, Y, threads=1):
        self.input_size = X.shape[1]
        self.output_size = Y.shape[1]
        X, Y = self.data_transform(X, Y)
        self.models = []
        fit_meta = {}
        for i, model_type in enumerate(self.pytorch_model_types):
            model = model_type(input_size=X.shape[1], output_size=Y.shape[1], **self.pytorch_model_arguments)
            trained_model, val_loss, train_loss = self.individual_model_train(model, X=X, Y=Y)
            self.models.append(trained_model)

            cb_meta = {f'validation_loss_{i}': val_loss, f'train_loss_{i}': train_loss}
            fit_meta = {**fit_meta, **cb_meta}
        return fit_meta

    @staticmethod
    def load_model(directory):
        meta = pickle.load(open(directory + "/meta.p", "rb"))
        m = import_from_class_name(meta['model_model_type'])()
        m.models = PytorchMultitaskModel.load_models(directory, meta)
        m.X_scale_transformer, m.Y_scale_transformer = PytorchMultitaskModel.load_transformers(directory)
        return m


def create_directory_if_not_exist(directory):
    Path(directory).mkdir(parents=True, exist_ok=True)


class PytorchModel(MOLModel, ABC, is_registered=False):
    def __init__(self, pytorch_model_types, pytorch_model_arguments=None):
        super().__init__()
        assert isinstance(pytorch_model_types, list)
        assert len(pytorch_model_types) > 0

        self.pytorch_model_types = pytorch_model_types
        if pytorch_model_arguments is None:
            pytorch_model_arguments = {}
        self.pytorch_model_arguments = pytorch_model_arguments
        self.Y_scale_transformer = None
        self.X_scale_transformer = None
        self.models = []
        self.epochs = 200
        self.batch_size = 256
        self.input_size = None
        self.output_size = None

    def predict(self, X):
        X = self.X_scale_transformer.transform(X)
        Xt = torch.from_numpy(X).float()
        X_list = []
        for j, column_models in enumerate(self.models):
            single_column_preds = [model(Xt).detach().numpy() for model in column_models]
            y = np.zeros_like(single_column_preds[0])
            for p in single_column_preds:
                y += p / len(column_models)
            X_list.append(y)
        s = np.hstack(X_list)
        s = self.Y_scale_transformer.inverse_transform(s)
        return s

    def save(self, directory):
        for j, column_models in enumerate(self.models):
            for i, model in enumerate(column_models):
                sub_dir = directory + f'/station_{j}'
                create_directory_if_not_exist(sub_dir)
                torch.save(model.state_dict(), sub_dir + f'/state_dict_model_{i}.pt')
        params = {'input_size': self.input_size, 'output_size': self.output_size, **self.pytorch_model_arguments}
        meta = {'model_model_type': get_class_name(self),
                'pytorch_model_type': [[get_class_name(x) for x in column_models] for column_models in self.models],
                'params': params}
        pickle.dump(meta, open(directory + "/meta.p", "wb"))
        self.save_transformers(directory)

    def save_transformers(self, directory):
        pickle.dump(self.Y_scale_transformer, open(directory + "/scala_transformet.p", "wb"))
        pickle.dump(self.X_scale_transformer, open(directory + "/X_scala_transformet.p", "wb"))

    @staticmethod
    def load_transformers(directory):
        Y_tr = pickle.load(open(directory + "/scala_transformet.p", "rb"))
        X_tr = pickle.load(open(directory + "/X_scala_transformet.p", "rb"))
        return X_tr, Y_tr

    @staticmethod
    def load_models(directory, meta):
        models = []
        for j, column_models in enumerate(meta['pytorch_model_type']):
            column_return_models = []
            for i, model_type in enumerate(column_models):
                meta_copy = meta['params'].copy()
                meta_copy['output_size'] = 1
                model = import_from_class_name(model_type)(**meta_copy)
                sub_dir = directory + f'/station_{j}'
                model.load_state_dict(torch.load(sub_dir + f'/state_dict_model_{i}.pt'))
                column_return_models.append(model)
            models.append(column_return_models)
        return models

    def individual_model_train(self, model, X=None, Y=None):
        data = PytorchDataset(X, Y)
        validation_size = int(len(data) * 0.1)
        train_size = len(data) - validation_size
        train_data, validation_data = torch.utils.data.random_split(data, [train_size, validation_size])
        train_dataloader = DataLoader(train_data, batch_size=self.batch_size, shuffle=True, drop_last=True)
        val_dataloader = DataLoader(validation_data, batch_size=self.batch_size, shuffle=True, drop_last=True)
        val_callback = MetricsCallback()
        train_callback = MyPrintingCallback(train_data)
        stopping_callback = EarlyStopping(monitor='validation_loss', patience=5)
        trainer = pl.Trainer(max_epochs=self.epochs,
                             progress_bar_refresh_rate=0,
                             callbacks=[val_callback, train_callback, stopping_callback])
        trainer.fit(model, train_dataloader=train_dataloader, val_dataloaders=val_dataloader)
        val_loss = val_callback.metrics['validation_loss'][1:]
        train_loss = train_callback.loss
        return model, val_loss, train_loss

    def data_transform(self, X, Y):
        self.Y_scale_transformer = MinMaxScaler()
        self.X_scale_transformer = MinMaxScaler()
        Y = self.Y_scale_transformer.fit_transform(Y)
        X = self.X_scale_transformer.fit_transform(X)
        return X, Y

    def fit(self, X, Y, threads=1):
        self.input_size = X.shape[1]
        self.output_size = Y.shape[1]

        X, Y = self.data_transform(X, Y)
        self.models = []
        fit_meta = {}

        for column_number in range(self.output_size):
            column_models = []
            for i, model_type in enumerate(self.pytorch_model_types):
                model = model_type(input_size=X.shape[1], output_size=1, **self.pytorch_model_arguments)
                output_data = np.expand_dims(Y[:, column_number], axis=1)
                trained_model, val_loss, train_loss = self.individual_model_train(model, X=X, Y=output_data)
                column_models.append(trained_model)
                cb_meta = {f'validation_loss_{i}_column_{column_number}': val_loss,
                           f'train_loss_{i}_column_{column_number}': train_loss}
                fit_meta = {**fit_meta, **cb_meta}
            self.models.append(column_models)
        return fit_meta

    @staticmethod
    def load_model(directory):
        meta = pickle.load(open(directory + "/meta.p", "rb"))
        m = import_from_class_name(meta['model_model_type'])()
        m.models = PytorchModel.load_models(directory, meta)
        m.X_scale_transformer, m.Y_scale_transformer = PytorchModel.load_transformers(directory)
        return m


class PytorchRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 128, 'number_of_hidden_layers': 2})


class PytorchEnsembleRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=3 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 128, 'number_of_hidden_layers': 2})


class PytorchShallowRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 128, 'number_of_hidden_layers': 1})


class PytorchShallowEnsembleRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=3 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 128, 'number_of_hidden_layers': 1})


class PytorchDeepRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 128, 'number_of_hidden_layers': 3})


class PytorchDeepEnsembleRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=3 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 128, 'number_of_hidden_layers': 3})

        
class PytorchDeepDeepRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 128, 'number_of_hidden_layers': 4})


class PytorchDeepDeepEnsembleRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=3 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 128, 'number_of_hidden_layers': 4})
        
        
class PytorchDeepDeepDeepRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 128, 'number_of_hidden_layers': 5})


class PytorchDeepDeepDeepEnsembleRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=3 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 128, 'number_of_hidden_layers': 5})


class PytorchWideRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 256, 'number_of_hidden_layers': 2})


class PytorchWideEnsembleRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=3 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 256, 'number_of_hidden_layers': 2})


class PytorchWideShallowRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 256, 'number_of_hidden_layers': 1})


class PytorchWideShallowEnsembleRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=3 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 256, 'number_of_hidden_layers': 1})


class PytorchWideDeepRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 256, 'number_of_hidden_layers': 3})


class PytorchWideDeepEnsembleRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=3 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 256, 'number_of_hidden_layers': 3})
        

class PytorchWideDeepDeepRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 256, 'number_of_hidden_layers': 4})


class PytorchWideDeepDeepEnsembleRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=3 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 256, 'number_of_hidden_layers': 4})
        
        
class PytorchWideDeepDeepDeepRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 256, 'number_of_hidden_layers': 5})


class PytorchWideDeepDeepDeepEnsembleRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=3 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 256, 'number_of_hidden_layers': 5})
        


        
        
class PytorchWideWideRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 512, 'number_of_hidden_layers': 2})


class PytorchWideWideEnsembleRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=3 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 512, 'number_of_hidden_layers': 2})


class PytorchWideWideShallowRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 512, 'number_of_hidden_layers': 1})


class PytorchWideWideShallowEnsembleRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=3 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 512, 'number_of_hidden_layers': 1})


class PytorchWideWideDeepRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 512, 'number_of_hidden_layers': 3})


class PytorchWideWideDeepEnsembleRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=3 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 512, 'number_of_hidden_layers': 3})
        

class PytorchWideWideDeepDeepRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 512, 'number_of_hidden_layers': 4})


class PytorchWideWideDeepDeepEnsembleRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=3 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 512, 'number_of_hidden_layers': 4})

        
class PytorchWideWideDeepDeepDeepRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 512, 'number_of_hidden_layers': 5})


class PytorchWideWideDeepDeepDeepEnsembleRegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=3 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 512, 'number_of_hidden_layers': 5})
        
        
class PytorchLinearRegressionModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchLinearRegression],
                         pytorch_model_arguments={})

# ----------------------------------------- Ensembles(10) ----------------------------------------

class PytorchEnsemble10RegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=10 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 128, 'number_of_hidden_layers': 2})


class PytorchShallowEnsemble10RegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=10 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 128, 'number_of_hidden_layers': 1})


class PytorchDeepEnsemble10RegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=10 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 128, 'number_of_hidden_layers': 3})


class PytorchDeepDeepEnsemble10RegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=10 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 128, 'number_of_hidden_layers': 4})


class PytorchDeepDeepDeepEnsemble10RegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=10 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 128, 'number_of_hidden_layers': 5})


class PytorchWideEnsemble10RegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=10 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 256, 'number_of_hidden_layers': 2})


class PytorchWideShallowEnsemble10RegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=10 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 256, 'number_of_hidden_layers': 1})


class PytorchWideDeepEnsemble10RegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=10 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 256, 'number_of_hidden_layers': 3})


class PytorchWideDeepDeepEnsemble10RegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=10 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 256, 'number_of_hidden_layers': 4})


class PytorchWideDeepDeepDeepEnsemble10RegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=10 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 256, 'number_of_hidden_layers': 5})


class PytorchWideWideEnsemble10RegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=10 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 512, 'number_of_hidden_layers': 2})


class PytorchWideWideShallowEnsemble10RegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=10 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 512, 'number_of_hidden_layers': 1})


class PytorchWideWideDeepEnsemble10RegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=10 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 512, 'number_of_hidden_layers': 3})


class PytorchWideWideDeepDeepEnsemble10RegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=10 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 512, 'number_of_hidden_layers': 4})


class PytorchWideWideDeepDeepDeepEnsemble10RegressorModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=10 * [PytorchNetGeneral],
                         pytorch_model_arguments={'hidden_layer_size': 512, 'number_of_hidden_layers': 5})

# ----------------------------------------- Ensembles(10) ----------------------------------------

class PytorchNetNoDropoutModel(PytorchMultitaskModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral],
                         pytorch_model_arguments={'use_dropout': False})


class PytorchSingleRegressorModel(PytorchModel):
    def __init__(self):
        super().__init__(pytorch_model_types=[PytorchNetGeneral])


class PytorchSingleEnsembleRegressorModel(PytorchModel):
    def __init__(self):
        super().__init__(pytorch_model_types=3 * [PytorchNetGeneral])


class KerasModel(MOLModel, ABC, is_registered=False):
    @staticmethod
    def load_model(directory):
        return load_model(directory + '/model.h5')

    def __init__(self):
        self.model = None
        super().__init__()

    def save(self, directory):
        self.model.save(directory + '/model.h5')

    def set_cores(self, threads):
        tf.config.threading.set_intra_op_parallelism_threads(threads)
        tf.config.threading.set_inter_op_parallelism_threads(threads)


class KerasRegressorModel(KerasModel):
    @staticmethod
    def load_model(directory):
        model = KerasRegressorModel()
        meta = pickle.load(open(directory + "/meta.p", "rb"))
        model.model = get_keras_net(meta['input_shape'], meta['output_shape'])
        model.model.load_weights(directory + '/model')
        model.Y_scale_transformer = pickle.load(open(directory + "/scala_transformet.p", "rb"))
        return model

    def __init__(self):
        super().__init__()
        self.meta = None
        self.Y_scale_transformer = None

    def fit(self, X, Y, threads=1):
        self.set_cores(threads)
        self.model = get_keras_net(input_shape=X.shape[1], output_shape=Y.shape[1])
        self.meta = {'input_shape': X.shape[1], 'output_shape': Y.shape[1]}
        self.model.compile(loss='mae', optimizer='adam')

        self.Y_scale_transformer = MinMaxScaler()
        Y = self.Y_scale_transformer.fit_transform(Y)
        Y = np.nan_to_num(Y)

        self.model.fit(X, Y, epochs=70, batch_size=64, verbose=False)

    def predict(self, X):
        return self.Y_scale_transformer.inverse_transform(self.model.predict(X))

    def save(self, directory):
        assert self.model is not None, 'Model not yet trained'
        self.model.save_weights(directory + '/model')
        pickle.dump(self.meta, open(directory + "/meta.p", "wb"))
        pickle.dump(self.Y_scale_transformer, open(directory + "/scala_transformet.p", "wb"))


class KerasEnsembleRegressorModel(KerasModel):
    @staticmethod
    def load_model(directory):
        model = KerasEnsembleRegressorModel()
        meta = pickle.load(open(directory + "/meta.p", "rb"))
        model_ls = []
        for i in range(meta['ensemble_size']):
            new_model = get_keras_net(meta['input_shape'], meta['output_shape'])
            new_model.load_weights(directory + f'/model{i}')
            model_ls.append(new_model)
        model.models = model_ls
        model.Y_scale_transformer = pickle.load(open(directory + "/scala_transformet.p", "rb"))
        return model

    def __init__(self):
        super().__init__()
        self.meta = None
        self.ensemble_size = 3
        self.models = None
        self.Y_scale_transformer = None

    def fit(self, X, Y, threads=1):
        self.set_cores(threads)
        self.meta = {'input_shape': X.shape[1], 'output_shape': Y.shape[1], 'ensemble_size': self.ensemble_size}

        self.Y_scale_transformer = MinMaxScaler()
        Y = self.Y_scale_transformer.fit_transform(Y)
        Y = np.nan_to_num(Y)

        self.models = []
        for i in range(self.ensemble_size):
            model = get_keras_net(input_shape=X.shape[1], output_shape=Y.shape[1])
            model.compile(loss='mae', optimizer='adam')
            model.fit(X, Y, epochs=70, batch_size=64, verbose=False)
            self.models.append(model)

    def predict(self, X):
        preds = [self.Y_scale_transformer.inverse_transform(model.predict(X)) for model in self.models]
        Y = np.zeros_like(preds[0])
        for p in preds:
            Y += p / self.ensemble_size
        return Y

    def save(self, directory):
        assert self.models is not None, 'Model not yet trained'
        for i, model in enumerate(self.models):
            model.save_weights(directory + f'/model{i}')
        pickle.dump(self.meta, open(directory + "/meta.p", "wb"))
        pickle.dump(self.Y_scale_transformer, open(directory + "/scala_transformet.p", "wb"))


class KerasDistributionRegressorModel(KerasModel):
    @staticmethod
    def load_model(directory):
        model = KerasDistributionRegressorModel()
        meta = pickle.load(open(directory + "/meta.p", "rb"))
        model.model = get_keras_distribution_net(meta['input_shape'], meta['output_shape'])
        model.model.load_weights(directory + '/model')
        model.Y_scale_transformer = pickle.load(open(directory + "/scala_transformet.p", "rb"))
        return model

    def __init__(self):
        self.meta = None
        self.Y_scale_transformer = None
        super().__init__()

    def fit(self, X, Y, threads=1):
        self.set_cores(threads)
        self.model = get_keras_distribution_net(input_shape=X.shape[1], output_shape=Y.shape[1])
        self.meta = {'input_shape': X.shape[1], 'output_shape': Y.shape[1]}
        self.model.compile(loss=neg_log_likelihood, optimizer='adam')

        self.Y_scale_transformer = MinMaxScaler()
        Y = self.Y_scale_transformer.fit_transform(Y)
        Y = np.nan_to_num(Y)

        self.model.fit(X, Y, epochs=70, batch_size=64, verbose=False)

    def save(self, directory):
        assert self.model is not None, 'Model not yet trained'
        self.model.save_weights(directory + '/model')
        pickle.dump(self.meta, open(directory + "/meta.p", "wb"))
        pickle.dump(self.Y_scale_transformer, open(directory + "/scala_transformet.p", "wb"))

    def predict(self, X):
        # self.model(X).scale.numpy() returns std
        return self.Y_scale_transformer.inverse_transform(self.model(X).loc.numpy())


class XGBoostSingleModel(MOLModel):
    @staticmethod
    def load_model(directory):
        i = 0
        return_model = XGBoostSingleModel()
        while os.path.isfile(directory + f'/model_{i}.json'):
            m = XGBRegressor()
            m.load_model(directory + f'/model_{i}.json')
            return_model.models.append(m)
            i += 1
        return return_model

    def __init__(self):
        self.model_type = XGBRegressor
        self.models = []

    def predict(self, X):
        X_list = []
        for model in self.models:
            p = model.predict(X)
            X_list.append(p)
        s = np.vstack(X_list).T
        return s

    def train_model(self, X, y):
        model = self.model_type()
        model.fit(X, y)
        return model

    def fit(self, X, Y, threads=1):
        Y = np.nan_to_num(Y)
        self.models = [self.train_model(X, Y[:, i]) for i in range(Y.shape[1])]

    def save(self, directory):
        for i, model in enumerate(self.models):
            model.save_model(directory + f'/model_{i}.json')
