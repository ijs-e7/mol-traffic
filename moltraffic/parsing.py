import logging
import pandas as pd
from openpyxl import load_workbook
import datetime


def key_format(key):
    return encoding_filter(key.replace('.', ''))


def encoding_filter(x):
    return x.replace('è', 'c').replace('È', 'C').replace('È', 'C')


def keys_transform(keys):
    """
    Transforms a list of tuples (key, value) into a dictionary.
    If 2 or more keys are repeated, _num is appended

    >>> keys_transform(['A', 'B', 'A'])
    ['A_0', 'B', 'A_1']
    """
    new_keys = []
    count_ls = []
    for k in keys:
        if keys.count(k) > 1:
            counter = count_ls.count(k)
            count_ls.append(k)
            new_key = key_format(k + '_' + str(counter))
            new_keys.append(new_key)
        else:
            new_key = key_format(k)
            new_keys.append(new_key)
    return new_keys


def time_format_24_to_00(date_formatted):
    """
    Transform date from %d.%m.%Y 24:00 into %d+1.%m.%Y 00:00
    since any date ending with 24:00 is invalid

    >>> time_format_24_to_00('12.11.2016 24:00')
    '13.11.2016 00:00'

    >>> time_format_24_to_00('12.11.2016 23:00')
    '12.11.2016 23:00'
    """
    split = date_formatted.split(' ')
    date, time = split[0], split[1]
    if time == '24:00':
        new_date = datetime.datetime.strptime(date, '%d.%m.%Y')
        new_date += datetime.timedelta(days=1)
        new_str = new_date.strftime("%d.%m.%Y") + " 00:00"
        return new_str
    return date_formatted


def parse_minute_file(file_name, log_name=None):
    if log_name is not None:
        logging.basicConfig(filename=log_name, filemode='a', format='%(message)s')

    # Open with encoding cp1252
    with open(str(file_name), 'r', encoding='cp1252') as file:
        lines = [line.strip() for line in file.readlines()]

        # Find index of start and end data
        start_data_line = [i for i in range(len(lines)) if '--------------' in lines[i]][0]
        end_data_line = [i for i in range(len(lines)) if '==============' in lines[i]][0]

        station = ' '.join(lines[2].split('/')[1].strip().split(':')[1].strip().split()[1:])

        # Add STM element
        header = ['STM'] + lines[start_data_line - 2].split()
        header = keys_transform(header)

        header_short_0 = ['STM', 'DIR_0', 'DATUM', 'CAS', 'Err'] + [x for x in header if x.endswith('_0')]
        header_short_1 = ['STM', 'DIR_1', 'DATUM', 'CAS', 'Err'] + [x for x in header if x.endswith('_1')]
        header_short = [x.replace('_0', '') for x in header_short_0]

        direction0 = encoding_filter(lines[3])
        direction1 = encoding_filter(lines[4])

        # Create pandas data frames
        data_list = []
        for line_number in range(start_data_line + 1, end_data_line):
            try:
                line = lines[line_number].split()
                # Fix line split if there is no error
                if len(line) != len(header):
                    line.append('/')

                dict_map = {k: v for k, v in zip(header, line)}
                dict_map['DIR_0'] = direction0
                dict_map['DIR_1'] = direction1

                dict_map['STM'] = dict_map["STM"]

                ap_line_0 = [dict_map[k] for k in header_short_0]
                ap_line_1 = [dict_map[k] for k in header_short_1]

                data_list.append(ap_line_0)
                data_list.append(ap_line_1)
            except Exception:
                # Log exceptions with rows.
                if log_name is not None:
                    logging.warning(f'{file_name}: Error parsing line {line_number} ({lines[line_number]})')

        return_df = pd.DataFrame(data_list, columns=header_short)
        return_df['Time'] = pd.to_datetime(return_df['DATUM'] + ' ' + return_df['CAS'], format='%d.%m.%y %H:%M')
        return_df = return_df.drop(columns=['DATUM', 'CAS'])
        return_df['SRC'] = 'CSV'
        return_df['NAME'] = station
        return return_df


def parse_minute_file_excel(file, log_name=None):
    if log_name is not None:
        logging.basicConfig(filename=log_name, filemode='a', format='%(message)s')

    wb = load_workbook(file)

    # Ignore files with no sheet
    if '15 MINUTNI PODATKI' not in wb.sheetnames:
        if log_name is not None:
            logging.warning(f'{file}: No sheet 15 MINUTNI PODATKI')
        return None

    ws = wb['15 MINUTNI PODATKI']
    stm = ws['A3'].value.split('/')[1].strip().split(':')[1].strip().split(' ')[0]
    dir1 = ws['A4'].value.strip()
    dir2 = ws['A5'].value.strip()

    station = ' '.join(ws['A3'].value.split('/')[1].strip().split(':')[1].strip().split()[1:])

    xl_file = pd.ExcelFile(file)
    df = xl_file.parse('15 MINUTNI PODATKI', skipfooter=2,
                       skiprows=lambda x: x in [0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11])

    # Some files have different names for time column
    time_column_name = 'ČAS' if 'ČAS' in df.columns else 'Čas'

    dates = df['DATUM'].astype(str) + ' ' + df[time_column_name].astype(str)
    dates = dates.apply(time_format_24_to_00)

    # Exact is set to false since some dates have format '%d.%m.%Y %H:%M:%S'
    # Some dataframes end with 23:59:00 and not 23:45
    df['Time'] = pd.to_datetime(dates, format='%d.%m.%Y %H:%M', exact=False)
    df['Time'] = df['Time'].dt.round('15min')

    df['OCC'] = -1
    df['GAP'] = -1
    df['VMIN'] = -1
    df['VAVG'] = -1
    df['VMAX'] = -1
    df['STM'] = stm
    df['Err'] = '/'
    df['DIR.1'] = dir1
    df['DIR.2'] = dir2
    df['SRC'] = 'EXC'
    df['NAME'] = station

    df = df.drop(columns=['DATUM', time_column_name])

    df1_columns = [
        'STM', 'DIR.1', 'Err', 'Mot.', 'Osebni', 'BUS', 'L.Tov', 'S.Tov', 'T.Tov', 'T.S.P.', 'Vlač', 'OCC',
        'GAP', 'VMIN', 'VAVG', 'VMAX', 'Time', 'SRC', 'NAME'
    ]

    df2_columns = [
        'STM', 'DIR.2', 'Err', 'Mot..1', 'Osebni.1', 'BUS.1', 'L.Tov.1', 'S.Tov.1', 'T.Tov.1',
        'T.S.P..1', 'Vlač.1', 'OCC', 'GAP', 'VMIN', 'VAVG', 'VMAX', 'Time', 'SRC', 'NAME'
    ]

    df1 = df[df1_columns].copy()
    df2 = df[df2_columns].copy()

    df1_rename_dict = {'DIR.1': 'DIR', 'Mot.': 'Mot', 'L.Tov': 'LTov', 'S.Tov': 'STov',
                       'T.Tov': 'TTov', 'T.S.P.': 'TSP', 'Vlač': 'Vlac'}
    df2_rename_dict = {'DIR.2': 'DIR', 'Mot..1': 'Mot', 'Osebni.1': 'Osebni', 'BUS.1': 'BUS', 'L.Tov.1': 'LTov',
                       'S.Tov.1': 'STov',
                       'T.Tov.1': 'TTov', 'T.S.P..1': 'TSP', 'Vlač.1': 'Vlac'}

    df1.rename(columns=df1_rename_dict, inplace=True)
    df2.rename(columns=df2_rename_dict, inplace=True)
    return pd.concat([df1, df2])
